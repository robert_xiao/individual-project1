# Individual project1
## Overview
Continuous Delivery of Personal Website.
## Website Link:
https://robert-xiao-gitlab-io-robert-xiao-286cccc20b54c1f7a41a6bcb9d876.gitlab.io <br>
## Website Functionality & Demo Video：<br>
There are two sections on the website: my research CV and an IDS_projects section. Additionally, there is a link to the corresponding projects. <br>

The demo video named Demo.mp4 can be found in the root directory. <br>
.gitlab-ci.yml instructs GitLab Runner to create and deploy website to GitLab Pages. CI/CD pipelines publish and update automatically. .gitlab-ci.yml is shown below.<br>
![Alt text](image.png)

 
## GitLab Workflow, Hosting and Deployment:
Screenshots below show the GitLab workflow to build and deploy site on push for the passed pipeline.  <br>
![Alt text](image-1.png)
![Alt text](image-2.png)

The following screenshots demonstrate that the site hosted on Vercel is working successfully:  <br>
![Alt text](image-3.png)
Where the link: robert-xiao-gitlab-io.vercel.app<br>
## Website Screenshots:
![Alt text](image-4.png)
![Alt text](image-5.png)
![Alt text](image-6.png)
 
